2016-7-3 - Added QA to thermocouple readings due to unstable amplifier or
thermocouple readings.

2016-7-6 Removed the bracketing QA
        Added three more thermocouple readings
        Fixed the fan runtime
        Fixed the fan state truncation caused by too small of a buffer
        
